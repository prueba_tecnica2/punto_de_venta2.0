CREATE DATABASE puntoVenta;
USE DATABASE puntoVenta;


CREATE TABLE Productos (
    id_producto SERIAL PRIMARY KEY NOT NULL UNIQUE,
	codigo VARCHAR (20) NOT NULL UNIQUE,
    nombre VARCHAR(255) NOT NULL UNIQUE,
    precio DECIMAL(10, 2) NOT NULL,
	existencia BOOLEAN,
    stock INT NOT NULL
);

CREATE TABLE Perfiles (
    id_perfil SERIAL PRIMARY KEY NOT NULL UNIQUE,
    perfil VARCHAR(255) NOT NULL
);

CREATE TABLE Estados(
	id_estado SERIAL PRIMARY KEY NOT NULL UNIQUE,
	estado VARCHAR (30) NOT NULL UNIQUE
	);

CREATE TABLE Empleados (
    id_empleado SERIAL PRIMARY KEY NOT NULL UNIQUE,
	perfil_id INT NOT NULL REFERENCES Perfiles (id_perfil),
	nombre VARCHAR(255) NOT NULL ,
	usuario VARCHAR(50) NOT NULL UNIQUE,
    contrasena VARCHAR(100) NOT NULL,
	estado_id INT NOT NULL REFERENCES Estados (id_estado),
	UNIQUE (nombre,usuario)
);

CREATE TABLE Clientes (
    id_cliente SERIAL PRIMARY KEY NOT NULL UNIQUE,
    nombre VARCHAR(255) NOT NULL ,
    apellido VARCHAR(255) NOT NULL,
    correo_electronico VARCHAR(255) NOT NULL UNIQUE,
    telefono VARCHAR(10)NOT NULL UNIQUE,
	estado_id INT NOT NULL REFERENCES Estados (id_estado),
	UNIQUE (Nombre, Apellido)
);

CREATE TABLE Ventas (
    id_venta SERIAL PRIMARY KEY NOT NULL UNIQUE,
    fecha_hora_venta TIMESTAMP NOT NULL,
    cliente_id INT REFERENCES Clientes(ID_Cliente)NOT NULL,
    total_de_venta DECIMAL(10, 2) NOT NULL
);

CREATE TABLE Detalles_de_Ventas (
    id_detalle SERIAL PRIMARY KEY NOT NULL UNIQUE,
    venta_id INT REFERENCES Ventas(ID_Venta)NOT NULL,
    producto_id INT REFERENCES Productos(ID_Producto)NOT NULL,
    cantidad_vendida INT NOT NULL,
    precio_unitario DECIMAL(10, 2) NOT NULL
);

INSERT INTO Productos (codigo, Nombre, Precio,existencia, stock)
VALUES ('001', 'Producto A', 10.99,true, 100),
       ('002', 'Producto B', 19.99,true, 50),
       ('003', 'Producto C', 5.99,true, 200);
	   
INSERT INTO Clientes (Nombre, Apellido, Correo_electronico, Telefono,estado_id)
VALUES ('nombre 1', 'apellido 1', 'correo_1@example.com', '1111111111',1),
       ('combre 2', 'apellido 2', 'correo_2@example.com', '2222222222',1);
	   
	   
INSERT INTO Empleados (perfil_id,nombre,usuario,contrasena,estado_id)
VALUES (1,'nombre 1','usuario 1','contraseña_1',1),
       (1,'nombre 2','usuario 2','contraseña_2',1);
	 
	 
	   
select * from clientes c ;
select * from empleados e ;
select * from productos p ;
select * from ventas v ;
select * from detalles_de_ventas ddv ;













